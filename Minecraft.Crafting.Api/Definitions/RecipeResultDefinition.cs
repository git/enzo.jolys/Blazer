﻿namespace Minecraft.Crafting.Api.Definitions
{
    using System.Text.Json.Serialization;

    public class RecipeResultDefinition
    {
        [JsonPropertyName("count")]
        public long Count { get; set; }

        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("metadata")]
        public long Metadata { get; set; }
    }
}
