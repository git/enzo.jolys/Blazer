﻿namespace Minecraft.Crafting.Api.Definitions
{
    using System.Text.Json.Serialization;

    public class RecipeDefinition
    {
        //[JsonPropertyName("ingredients")]
        //public IngredientElement[] Ingredients { get; set; }

        [JsonPropertyName("result")]
        public RecipeResultDefinition Result { get; set; }

        [JsonPropertyName("inShape")]
        public RecipeInShapeDefinition[][] InShape { get; set; }

        //[JsonPropertyName("outShape")]
        //public long?[][] OutShape { get; set; }
    }
}
