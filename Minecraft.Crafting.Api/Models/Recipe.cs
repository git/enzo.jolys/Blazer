﻿namespace Minecraft.Crafting.Api
{
    using Minecraft.Crafting.Api.Models;

    public class Recipe
    {
        public Item Give { get; set; }
        public List<List<string>> Have { get; set; }
    }
}