namespace Minecraft.Crafting.Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Minecraft.Crafting.Api.Models;
    using System.Text.Json;
    using System.Text.Json.Serialization;

    [ApiController]
    [Route("api/[controller]")]
    public class CraftingController : ControllerBase
    {
        private readonly JsonSerializerOptions _jsonSerializerOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            WriteIndented = true,
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
        };

        [HttpPost]
        [Route("")]
        public Task Add(Item item)
        {
            var data = JsonSerializer.Deserialize<List<Item>>(System.IO.File.ReadAllText("Data/items.json"), _jsonSerializerOptions);

            data.Add(item);

            System.IO.File.WriteAllText("Data/items.json", JsonSerializer.Serialize(data, _jsonSerializerOptions));

            return Task.CompletedTask;
        }

        [HttpGet]
        [Route("count")]
        public Task<int> Count()
        {
            var data = JsonSerializer.Deserialize<List<Item>>(System.IO.File.ReadAllText("Data/items.json"), _jsonSerializerOptions);

            return Task.FromResult(data.Count());
        }

        [HttpDelete]
        [Route("{id}")]
        public Task Delete(int id)
        {
            var data = JsonSerializer.Deserialize<List<Item>>(System.IO.File.ReadAllText("Data/items.json"), _jsonSerializerOptions);

            var item = data?.FirstOrDefault(w => w.Id == id);

            if (item == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            data.Remove(item);

            System.IO.File.WriteAllText("Data/items.json", JsonSerializer.Serialize(data, _jsonSerializerOptions));

            return Task.CompletedTask;
        }

        [HttpGet]
        [Route("{id}")]
        public Task<Item> GetById(int id)
        {
            var data = JsonSerializer.Deserialize<List<Item>>(System.IO.File.ReadAllText("Data/items.json"), _jsonSerializerOptions);

            var item = data?.FirstOrDefault(w => w.Id == id);

            if (item == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            return Task.FromResult(item);
        }

        [HttpGet]
        [Route("recipe")]
        public Task<List<Recipe>> GetRecipe()
        {
            if (!System.IO.File.Exists("Data/convert-recipes.json"))
            {
                ConvertRecipes();
            }
            
            var data = JsonSerializer.Deserialize<List<Recipe>>(System.IO.File.ReadAllText("Data/convert-recipes.json"), _jsonSerializerOptions);
            return Task.FromResult(data);
        }

        private void ConvertRecipes()
        {
            var data = JsonSerializer.Deserialize<List<Item>>(System.IO.File.ReadAllText("Data/items.json"), _jsonSerializerOptions);

            if (data == null)
            {
                return;
            }

            var recipes = Recipes.FromJson(System.IO.File.ReadAllText("Data/recipes.json"));

            var items = new List<Recipe>();

            foreach (var recipe in recipes.SelectMany(s => s.Value))
            {
                if (recipe.InShape == null)
                {
                    continue;
                }

                var giveItem = data.FirstOrDefault(w => w.Id == recipe.Result.Id);

                if (giveItem == null)
                {
                    continue;
                }

                items.Add(new Recipe
                {
                    Give = new Item { DisplayName = giveItem.DisplayName, Name = giveItem.Name },
                    Have = new List<List<string>>
                    {
                        new()
                        {
                            GetItemName(data, recipe.InShape, 0, 0),
                            GetItemName(data, recipe.InShape, 0, 1),
                            GetItemName(data, recipe.InShape, 0, 2)
                        },
                        new()
                        {
                            GetItemName(data, recipe.InShape, 1, 0),
                            GetItemName(data, recipe.InShape, 1, 1),
                            GetItemName(data, recipe.InShape, 1, 2)
                        },
                        new()
                        {
                            GetItemName(data, recipe.InShape, 2, 0),
                            GetItemName(data, recipe.InShape, 2, 1),
                            GetItemName(data, recipe.InShape, 2, 2)
                        }
                    }
                });
            }
            
            System.IO.File.WriteAllText("Data/convert-recipes.json", JsonSerializer.Serialize(items, _jsonSerializerOptions));
        }


        [HttpGet]
        [Route("")]
        public Task<List<Item>> List(int currentPage, int pageSize)
        {
            var data = JsonSerializer.Deserialize<List<Item>>(System.IO.File.ReadAllText("Data/items.json"), _jsonSerializerOptions);

            return Task.FromResult(data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList());
        }

        [HttpPut]
        [Route("{id}")]
        public Task Update(int id, Item item)
        {
            var data = JsonSerializer.Deserialize<List<Item>>(System.IO.File.ReadAllText("Data/items.json"), _jsonSerializerOptions);

            var itemOriginal = data?.FirstOrDefault(w => w.Id == id);

            if (itemOriginal == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            itemOriginal.Id = item.Id;
            itemOriginal.Name = item.Name;
            itemOriginal.CreatedDate = item.CreatedDate;
            itemOriginal.DisplayName = item.DisplayName;
            itemOriginal.EnchantCategories = item.EnchantCategories;
            itemOriginal.MaxDurability = item.MaxDurability;
            itemOriginal.RepairWith = item.RepairWith;
            itemOriginal.StackSize = item.StackSize;
            itemOriginal.UpdatedDate = item.UpdatedDate;

            System.IO.File.WriteAllText("Data/items.json", JsonSerializer.Serialize(data, _jsonSerializerOptions));

            return Task.CompletedTask;
        }

        private static string GetItemName(List<Item> items, InShape[][] inShape, int line, int row)
        {
            if (inShape.Length < line + 1)
            {
                return null;
            }

            if (inShape[line].Length < row + 1)
            {
                return null;
            }

            var id = inShape[line][row].Integer ?? inShape[line][row].IngredientClass?.Id;

            if (id == null)
            {
                return null;
            }

            return GetItemName(items, id.Value);
        }

        private static string GetItemName(List<Item> items, long id)
        {
            var item = items.FirstOrDefault(w => w.Id == id);
            return item?.Name;
        }
    }
}