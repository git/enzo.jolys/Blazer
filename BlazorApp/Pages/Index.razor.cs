﻿using BlazorApp.Components;
using BlazorApp.Models;
using Microsoft.AspNetCore.Components;

namespace BlazorApp.Pages
{
    public partial class Index
    {
        [Inject]
        public IDataService DataService { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        private List<CraftingRecipe> Recipes { get; set; } = new List<CraftingRecipe>();

        protected async Task OnAfterRenderAsync()
        {
            await base.OnInitializedAsync();

            Items = await DataService.List(0, await DataService.Count());
            Recipes = await DataService.GetRecipes();
        }


        public List<Cake> Cakes { get; set; }

        public void LoadCakes()
        {
            Cakes = new List<Cake>
        {
            // items hidden for display purpose
            new Cake
            {
                Id = 1,
                Name = "Red Velvet",
                Cost = 60
            },
        };
        }

        private Cake CakeItem = new Cake
        {
            Id = 1,
            Name = "Black Forest",
            Cost = 50
        };
    }
}
