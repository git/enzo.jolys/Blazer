﻿using BlazorApp.Factories;
using BlazorApp.Models;

namespace BlazorApp.Services
{
    public class DataApiServiceBase
    {

        public async Task Update(int id, ItemModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            await _http.PutAsJsonAsync($"https://localhost:7234/api/Crafting/{id}", item);
        }

        public Task Update(int id, ItemModel model)
        {
            throw new NotImplementedException();
        }
    }
}